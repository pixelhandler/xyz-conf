##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# http://wiki.nginx.org/Pitfalls
# http://wiki.nginx.org/QuickStart
# http://wiki.nginx.org/Configuration
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

server {
	listen 80;

	charset UTF-8;

	server_name qayaq.local;

	location / {
		root /usr/local/var/www/qayaq/xyz-foundation/dist;
	}

	location /hotels {
		alias /usr/local/var/www/qayaq/xyz-hotels/dist;
		index index.html;
		try_files $uri /hotels/index.html;
		add_header Cache-Control "no-cache, no-store, max-age=0";
	}

	location /flights {
		alias /usr/local/var/www/qayaq/xyz-flights/dist;
		index index.html;
		try_files $uri /flights/index.html;
		add_header Cache-Control "no-cache, no-store, max-age=0";
	}

	location /cars {
		alias /usr/local/var/www/qayaq/xyz-cars/dist;
		index index.html;
		try_files $uri /cars/index.html;
		add_header Cache-Control "no-cache, no-store, max-age=0";
	}

	location /trips {
		alias /usr/local/var/www/qayaq/xyz-trips/dist;
		index index.html;
		try_files $uri /trips/index.html;
		add_header Cache-Control "no-cache, no-store, max-age=0";
	}

	location ~* ^/api/(.*) {
		set $api_host          "127.0.0.1:3000";
		set $url_full          "$1";
		proxy_http_version     1.1;
		proxy_set_header       X-Real-IP $remote_addr;
		proxy_set_header       X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header       X-NginX-Proxy true;
		proxy_set_header       Upgrade $http_upgrade;
		proxy_set_header       Connection 'upgrade';
		proxy_set_header       Host $api_host;
		proxy_cache_bypass     $http_upgrade;
		proxy_redirect off;
		proxy_ssl_session_reuse off;
		resolver               8.8.4.4 8.8.8.8 valid=300s;
		resolver_timeout       10s;
		proxy_pass             $scheme://$api_host/api/$1$is_args$args;
	}

}
